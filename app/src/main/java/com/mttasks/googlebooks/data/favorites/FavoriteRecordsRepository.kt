package com.mttasks.googlebooks.data.favorites

import com.mttasks.googlebooks.data.favorites.database.FavoriteRecordsDao
import com.mttasks.googlebooks.domain.favorites.FavoriteRecordsRepository
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class FavoriteRecordsRepository @Inject constructor(
    private val favoriteRecordsDao: FavoriteRecordsDao
) : FavoriteRecordsRepository {

    override fun add(record: com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity) =
        favoriteRecordsDao.add(record)

    override fun remove(record: com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity) =
        favoriteRecordsDao.delete(record.uid)

    override fun check(ids: List<String>) = favoriteRecordsDao.check(ids)

    override fun all() = favoriteRecordsDao.all()

    override fun count() = favoriteRecordsDao.count()
}
