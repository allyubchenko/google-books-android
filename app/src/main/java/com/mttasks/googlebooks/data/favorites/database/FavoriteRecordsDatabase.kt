package com.mttasks.googlebooks.data.favorites.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Database(
    entities = [
        com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class FavoriteRecordsDatabase : RoomDatabase() {

    abstract fun getFavoriteRecordsDao(): FavoriteRecordsDao

}