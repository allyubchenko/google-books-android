package com.mttasks.googlebooks.data.search.network

import com.google.gson.annotations.SerializedName
import com.mttasks.googlebooks.data.search.network.dto.BookDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
interface BooksApiService {

    @GET("volumes")
    fun search(
        @Query("q") query: String,
        @Query("key") apiKey: String
    ): Single<BaseResponseDto<BookDto>>

}

class BaseResponseDto<T>(
    @SerializedName("items") val items: List<T>?
)