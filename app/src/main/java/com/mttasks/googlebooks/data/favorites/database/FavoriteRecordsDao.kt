package com.mttasks.googlebooks.data.favorites.database

import androidx.room.*
import com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity
import io.reactivex.Flowable
import io.reactivex.Single


/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Dao
abstract class FavoriteRecordsDao {

    @Transaction
    @Query("SELECT * FROM favorite_records")
    abstract fun all(): Flowable<List<FavoriteRecordEntity>>

    @Transaction
    @Query("SELECT uid FROM favorite_records where uid IN (:uidList)")
    abstract fun check(uidList: List<String>): Flowable<List<String>>

    @Query("SELECT COUNT(id) FROM favorite_records")
    abstract fun count(): Flowable<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun add(entity: FavoriteRecordEntity): Single<Long>

    @Transaction
    @Query("DELETE FROM favorite_records where uid = :uid")
    abstract fun delete(uid: String): Single<Int>

}