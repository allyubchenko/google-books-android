package com.mttasks.googlebooks.data.search.network.dto

import com.google.gson.annotations.SerializedName

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BookDto(
    @SerializedName("id") val id: String,
    @SerializedName("volumeInfo") val volumeInfo: VolumeInfoDto
)

class VolumeInfoDto(
    @SerializedName("title") val title: String,
    @SerializedName("authors") val authors: List<String>?,
    @SerializedName("imageLinks") val imageLinks: ImageLinksDto?,
    @SerializedName("previewLink") val previewUrl: String
)

class ImageLinksDto(
    @SerializedName("thumbnail") val thumbnail: String?,
    @SerializedName("smallThumbnail") val smallThumbnail: String?
)