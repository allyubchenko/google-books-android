package com.mttasks.googlebooks.data.search.datasource

import com.mttasks.googlebooks.data.search.network.BooksApiService
import com.mttasks.googlebooks.di.search.DiConstants
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Named

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BooksNetworkDataSource @Inject constructor(
    private val booksApiService: BooksApiService,
    @Named(DiConstants.DI_GOOGLE_API_KEY) private val googleApiKey: String
) {

    fun search(term: String): Single<List<com.mttasks.googlebooks.domain.common.model.Book>> =
        booksApiService.search(
        term,
        googleApiKey
    ).map { dtoList ->
        dtoList.items?.map { book ->
            com.mttasks.googlebooks.domain.common.model.Book(
                book.id,
                book.volumeInfo.imageLinks?.let { it.thumbnail ?: it.smallThumbnail },
                book.volumeInfo.title,
                book.volumeInfo.authors ?: emptyList(),
                book.volumeInfo.previewUrl
            )
        } ?: emptyList()
    }

}