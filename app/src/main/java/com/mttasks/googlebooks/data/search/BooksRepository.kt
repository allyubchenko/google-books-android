package com.mttasks.googlebooks.data.search

import com.mttasks.googlebooks.data.search.datasource.BooksNetworkDataSource
import com.mttasks.googlebooks.domain.search.BooksRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BooksRepository @Inject constructor(
    private val networkDataSource: BooksNetworkDataSource
) : BooksRepository {

    override fun search(term: String): Single<List<com.mttasks.googlebooks.domain.common.model.Book>> =
        networkDataSource.search(term)

}