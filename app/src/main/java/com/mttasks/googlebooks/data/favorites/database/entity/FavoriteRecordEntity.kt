package com.mttasks.googlebooks.data.favorites.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Entity(
    tableName = "favorite_records",
    indices = [Index(value = ["uid"], unique = true)]
)
class FavoriteRecordEntity(
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "uid") val uid: String,
    @ColumnInfo(name = "thumb_url") val thumbUrl: String?,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "authors") val authors: List<String>,
    @ColumnInfo(name = "bookFragmentUrl") val bookFragmentUrl: String
)