package com.mttasks.googlebooks.di

import androidx.lifecycle.ViewModelProvider
import com.mttasks.googlebooks.di.favorites.BooksFavoriteModule
import com.mttasks.googlebooks.di.search.BooksSearchModule
import com.mttasks.googlebooks.presentation.favorites.BooksFavoriteActivity
import com.mttasks.googlebooks.presentation.search.BooksSearchActivity
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Suppress("unused")
@Module
abstract class BindingModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [BooksSearchModule::class])
    internal abstract fun booksSearchActivity(): BooksSearchActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [BooksFavoriteModule::class])
    internal abstract fun booksFavoriteActivity(): BooksFavoriteActivity

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}