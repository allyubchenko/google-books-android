package com.mttasks.googlebooks.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Module
class ApplicationModule(private val application: Application) {
    @Provides
    @Singleton
    fun provideContext(): Context = application
}