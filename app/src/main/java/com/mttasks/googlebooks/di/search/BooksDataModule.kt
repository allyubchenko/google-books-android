package com.mttasks.googlebooks.di.search

import com.mttasks.googlebooks.BuildConfig
import com.mttasks.googlebooks.data.search.network.BooksApiService
import com.mttasks.googlebooks.domain.search.BooksRepository
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Module
class BooksDataModule {

    @Provides
    @Singleton
    fun provideRetrofit(): BooksApiService = Retrofit.Builder()
        .baseUrl("https://www.googleapis.com/books/v1/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(OkHttpClient.Builder().apply {
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            }
        }.build())
        .build()
        .create(BooksApiService::class.java)

    @Provides
    @Singleton
    @Named(DiConstants.DI_GOOGLE_API_KEY)
    fun provideGoogleApiKey(): String = BuildConfig.GOOGLE_API_KEY

    @Provides
    @Singleton
    fun provideBooksRepository(booksRepository: com.mttasks.googlebooks.data.search.BooksRepository): BooksRepository =
        booksRepository

}