/* Intellectual property of Lyubchenko Alexander
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.mttasks.googlebooks.di.search

/**
 * Created by Alexander Lyubchenko <alexander775412@gmail.com> on 2/11/21
 */
class DiConstants {

    companion object {
        const val DI_GOOGLE_API_KEY = "di_google_api_key"
    }

}