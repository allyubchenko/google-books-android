package com.mttasks.googlebooks.di.search

import androidx.lifecycle.ViewModel
import com.mttasks.googlebooks.di.ViewModelKey
import com.mttasks.googlebooks.presentation.search.fragment.SearchBooksListFragment
import com.mttasks.googlebooks.presentation.search.viewmodel.SearchBooksListViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.SearchBooksViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Suppress("unused")
@Module
abstract class BooksSearchModule {

    @Binds
    @IntoMap
    @ViewModelKey(SearchBooksViewModel::class)
    abstract fun bindSearchBooksViewModel(viewModel: SearchBooksViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchBooksListViewModel::class)
    abstract fun bindSearchBooksListViewModel(viewModel: SearchBooksListViewModel): ViewModel

    @ContributesAndroidInjector
    abstract fun contributeSearchBooksListFragment(): SearchBooksListFragment

}