package com.mttasks.googlebooks.di.favorites

import androidx.lifecycle.ViewModel
import com.mttasks.googlebooks.di.ViewModelKey
import com.mttasks.googlebooks.presentation.favorites.fragment.FavoriteRecordsListFragment
import com.mttasks.googlebooks.presentation.favorites.viewmodel.FavoriteRecordsListViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Suppress("unused")
@Module
abstract class BooksFavoriteModule {

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteRecordsListViewModel::class)
    abstract fun bindFavoriteRecordsListViewModel(viewModel: FavoriteRecordsListViewModel): ViewModel

    @ContributesAndroidInjector
    abstract fun contributeFavoriteRecordsListFragment(): FavoriteRecordsListFragment

}