package com.mttasks.googlebooks.di.favorites

import android.content.Context
import androidx.room.Room
import com.mttasks.googlebooks.data.favorites.FavoriteRecordsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Module
class FavoriteRecordsDataModule(private val databaseName: String) {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): com.mttasks.googlebooks.data.favorites.database.FavoriteRecordsDatabase =
        Room.databaseBuilder(
            context,
            com.mttasks.googlebooks.data.favorites.database.FavoriteRecordsDatabase::class.java,
            databaseName
        ).fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideFavoriteRecordsDao(db: com.mttasks.googlebooks.data.favorites.database.FavoriteRecordsDatabase): com.mttasks.googlebooks.data.favorites.database.FavoriteRecordsDao =
        db.getFavoriteRecordsDao()

    @Provides
    @Singleton
    fun provideFavoriteRecordsRepository(favoriteRecordsRepository: FavoriteRecordsRepository): com.mttasks.googlebooks.domain.favorites.FavoriteRecordsRepository =
        favoriteRecordsRepository

}