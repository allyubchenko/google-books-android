package com.mttasks.googlebooks.di

import javax.inject.Scope

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope