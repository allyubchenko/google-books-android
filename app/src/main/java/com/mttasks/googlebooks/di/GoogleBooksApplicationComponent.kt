package com.mttasks.googlebooks.di

import com.mttasks.googlebooks.GoogleBooksApplication
import com.mttasks.googlebooks.di.favorites.FavoriteRecordsDataModule
import com.mttasks.googlebooks.di.search.BooksDataModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        BindingModule::class,

        BooksDataModule::class,
        FavoriteRecordsDataModule::class
    ]
)
interface GoogleBooksApplicationComponent : AndroidInjector<GoogleBooksApplication> {

    @Component.Builder
    interface Builder {

        fun applicationModule(applicationModule: ApplicationModule): Builder

        fun favoriteRecordsDataModule(favoriteRecordsDataModule: FavoriteRecordsDataModule): Builder

        fun build(): GoogleBooksApplicationComponent

    }

    override fun inject(app: GoogleBooksApplication)
}