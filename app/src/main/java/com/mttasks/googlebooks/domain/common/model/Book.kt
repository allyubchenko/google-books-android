package com.mttasks.googlebooks.domain.common.model

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class Book(
    val id: String,
    val thumbUrl: String?,
    val title: String,
    val authors: List<String>,
    val bookFragmentUrl: String
)