package com.mttasks.googlebooks.domain.favorites

import com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity
import io.reactivex.Flowable
import io.reactivex.Single

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
interface FavoriteRecordsRepository {

    fun add(record: FavoriteRecordEntity): Single<Long>

    fun remove(record: FavoriteRecordEntity): Single<Int>

    fun check(ids: List<String>): Flowable<List<String>>

    fun all(): Flowable<List<FavoriteRecordEntity>>

    fun count(): Flowable<Int>
}