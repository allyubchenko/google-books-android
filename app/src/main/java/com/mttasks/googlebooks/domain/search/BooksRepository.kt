package com.mttasks.googlebooks.domain.search

import com.mttasks.googlebooks.domain.common.model.Book
import io.reactivex.Single

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
interface BooksRepository {

    fun search(term: String): Single<List<Book>>

}