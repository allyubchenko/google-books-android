package com.mttasks.googlebooks.domain.favorites.interactor

import com.mttasks.googlebooks.domain.common.model.Book
import com.mttasks.googlebooks.domain.favorites.FavoriteRecordsRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class ManageFavoriteRecordsUseCase @Inject constructor(private val favoriteRecordsRepository: FavoriteRecordsRepository) {

    fun add(record: Book): Single<Long> = favoriteRecordsRepository.add(record.toFavoriteRecord())

    fun remove(record: Book): Single<Int> =
        favoriteRecordsRepository.remove(record.toFavoriteRecord())

    fun check(idList: List<String>) = favoriteRecordsRepository.check(idList)

    fun count() = favoriteRecordsRepository.count()

    fun all() = favoriteRecordsRepository.all()

}

fun Book.toFavoriteRecord() =
    com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity(
        uid = this.id,
        thumbUrl = this.thumbUrl,
        title = this.title,
        authors = this.authors,
        bookFragmentUrl = this.bookFragmentUrl
    )