package com.mttasks.googlebooks.domain.search.interactor

import com.mttasks.googlebooks.domain.search.BooksRepository
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class SearchBooksUseCase @Inject constructor(private val booksRepository: BooksRepository) {

    fun search(term: String) = booksRepository.search(term)

}