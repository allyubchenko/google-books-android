package com.mttasks.googlebooks.presentation.favorites.view.listitem

import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.ViewBookFavoriteBinding
import com.mttasks.googlebooks.presentation.search.viewmodel.view.BookItemViewModel
import com.xwray.groupie.databinding.BindableItem

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BookFavoriteListItem(private val viewModel: BookItemViewModel) :
    BindableItem<ViewBookFavoriteBinding>() {

    override fun getId(): Long {
        return viewModel.book.id.hashCode().toLong()
    }

    override fun getLayout() = R.layout.view_book_favorite

    override fun bind(viewBinding: ViewBookFavoriteBinding, position: Int) {
        viewBinding.vm = viewModel
    }

}