package com.mttasks.googlebooks.presentation.common.viewmodel.view

import androidx.lifecycle.ViewModel
import com.mttasks.googlebooks.presentation.common.AppString

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class MessageViewModel(val message: AppString) : ViewModel()
