package com.mttasks.googlebooks.presentation.common.view.listitem

import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.ViewMessageBinding
import com.mttasks.googlebooks.presentation.common.viewmodel.view.MessageViewModel
import com.xwray.groupie.databinding.BindableItem

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class MessageListItem(private val errorViewModel: MessageViewModel) :
    BindableItem<ViewMessageBinding>() {
    override fun getLayout() = R.layout.view_message

    override fun bind(viewBinding: ViewMessageBinding, position: Int) {
        viewBinding.vm = errorViewModel
    }
}