package com.mttasks.googlebooks.presentation.common

import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.mttasks.googlebooks.R
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
inline fun <reified T : ViewModel> FragmentActivity.viewModel(
    factory: ViewModelProvider.Factory,
    key: String? = null
): T = if (key != null) {
    ViewModelProvider(this, factory).get(key, T::class.java)
} else {
    ViewModelProvider(this, factory).get(T::class.java)
}

inline fun <reified T : ViewModel> Fragment.viewModel(
    factory: ViewModelProvider.Factory,
    key: String? = null
): T = if (key != null) {
    ViewModelProvider(this, factory).get(key, T::class.java)
} else {
    ViewModelProvider(this, factory).get(T::class.java)
}

fun FragmentManager.withTransaction(block: FragmentTransaction.() -> Unit) =
    with(beginTransaction()) {
        this.block()
        this.commit()
    }

@Suppress("unused")
fun Throwable.toMessage() =
    AppString { getString(R.string.error_unknown) }

fun Throwable.log() = Timber.d(this)

fun <T> Single<T>.apiSubscribe(onSuccess: (T?) -> Unit, onError: (Throwable?) -> Unit): Disposable {
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(onSuccess, onError)
}

fun String.browserIntent() = AppIntent {
    Intent(
        Intent.ACTION_VIEW,
        Uri.parse(this@browserIntent)
    )
}

fun RecyclerView.dividerDecorator(orientation: Int) {
    addItemDecoration(DividerItemDecoration(this.context, orientation))
}