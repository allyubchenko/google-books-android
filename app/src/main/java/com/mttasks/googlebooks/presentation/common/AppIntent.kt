package com.mttasks.googlebooks.presentation.common

import android.content.Context
import android.content.Intent

/**
 *  <p>Created by Alexander Lyubchenko &lt;lyubchenkoaa@sibedge.com&gt; on 31.01.2020.</p>
 */
class AppIntent(private val provide: Context.() -> Intent) {

    fun get(context: Context) = context.provide()

}