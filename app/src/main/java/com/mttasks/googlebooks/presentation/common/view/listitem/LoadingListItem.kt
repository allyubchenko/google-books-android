package com.mttasks.googlebooks.presentation.common.view.listitem

import com.mttasks.googlebooks.R
import com.xwray.groupie.kotlinandroidextensions.GroupieViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class LoadingListItem : Item() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {}

    override fun getLayout() = R.layout.view_loading

}