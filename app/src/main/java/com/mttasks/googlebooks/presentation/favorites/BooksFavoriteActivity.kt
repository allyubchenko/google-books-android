package com.mttasks.googlebooks.presentation.favorites

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.ActivityFragmentBinding
import com.mttasks.googlebooks.presentation.common.withTransaction
import com.mttasks.googlebooks.presentation.favorites.fragment.FavoriteRecordsListFragment
import dagger.android.support.DaggerAppCompatActivity


/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BooksFavoriteActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<ActivityFragmentBinding>(this, R.layout.activity_fragment)
            .apply {
                lifecycleOwner = this@BooksFavoriteActivity

                setSupportActionBar(toolbar)
                toolbar.setNavigationOnClickListener { onSupportNavigateUp() }

                if (savedInstanceState == null) {
                    supportFragmentManager.withTransaction {
                        replace(
                            R.id.fragmentContainer,
                            FavoriteRecordsListFragment()
                        )
                    }
                }
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
//        return super.onSupportNavigateUp()
    }
}