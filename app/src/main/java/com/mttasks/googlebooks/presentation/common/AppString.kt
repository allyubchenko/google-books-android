package com.mttasks.googlebooks.presentation.common

import android.content.Context

/**
 *  <p>Created by Alexander Lyubchenko &lt;lyubchenkoaa@sibedge.com&gt; on 31.01.2020.</p>
 */
class AppString(private val provide: Context.() -> String) {

    fun get(context: Context) = context.provide()

}