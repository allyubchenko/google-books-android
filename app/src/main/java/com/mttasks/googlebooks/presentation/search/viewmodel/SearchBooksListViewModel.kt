package com.mttasks.googlebooks.presentation.search.viewmodel

import androidx.lifecycle.*
import com.hadilq.liveevent.LiveEvent
import com.mttasks.googlebooks.domain.common.model.Book
import com.mttasks.googlebooks.domain.favorites.interactor.ManageFavoriteRecordsUseCase
import com.mttasks.googlebooks.domain.search.interactor.SearchBooksUseCase
import com.mttasks.googlebooks.presentation.common.AppIntent
import com.mttasks.googlebooks.presentation.common.apiSubscribe
import com.mttasks.googlebooks.presentation.common.browserIntent
import com.mttasks.googlebooks.presentation.common.log
import com.mttasks.googlebooks.presentation.search.viewmodel.view.BookItemViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.view.toViewModelList
import io.reactivex.disposables.Disposable
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class SearchBooksListViewModel @Inject constructor(
    private val searchBooksUseCase: SearchBooksUseCase,
    private val favoriteRecordsUseCase: ManageFavoriteRecordsUseCase
) : ViewModel() {

    private var lastSearch: String? = null
    private var request: Disposable? = null

    private val _inProgress = MutableLiveData<Boolean>()
    val inProgress: LiveData<Boolean> = _inProgress

    private val _error = MutableLiveData<Throwable>()
    val error: LiveData<Throwable> = _error

    private val _databaseError = LiveEvent<Throwable>()
    val databaseError: LiveData<Throwable> = _databaseError

    private var favoritesIdStream: LiveData<List<String>>? = null
    private var searchResultRaw =
        MutableLiveData<List<Book>>()
    val searchResult: LiveData<List<BookItemViewModel>> =
        MediatorLiveData<List<BookItemViewModel>>().apply searchResult@{
            addSource(searchResultRaw) { rawResult ->
                val newValue = rawResult?.toViewModelList(
                    clickListener = { readBook(it.book) },
                    favoriteClickListener = { toggleFavorite(it) }
                )
                this.value = newValue

                favoritesIdStream?.let { this@searchResult.removeSource(it) }
                rawResult?.takeIf { it.isNullOrEmpty().not() }?.let { res ->
                    favoritesIdStream = LiveDataReactiveStreams.fromPublisher(
                        favoriteRecordsUseCase.check(res.map { it.id })
                            .onErrorReturn {
                                _databaseError.postValue(it)
                                emptyList()
                            }).apply {
                        this@searchResult.addSource(this) { favoritesIdList ->
                            val favoritesIdSet = favoritesIdList?.toSet() ?: emptySet()
                            newValue?.forEach { it.isFavorite.set(favoritesIdSet.contains(it.book.id)) }
                        }
                    }
                }
            }
    }

    private val _startActivity = LiveEvent<AppIntent>()
    val startActivity: LiveData<AppIntent> = _startActivity

    fun search(searchTerm: String?) {
        if (lastSearch != searchTerm) {
            request?.let { r ->
                _inProgress.value = false
                r.dispose()
                request = null
            }

            if (searchTerm?.isNotEmpty() == true) {
                lastSearch = searchTerm

                _error.value = null
                _inProgress.value = true

                request = searchBooksUseCase.search(searchTerm)
                    .apiSubscribe(
                        { result ->
                            _inProgress.value = false

                            searchResultRaw.value = result
                        },
                        { t ->
                            _inProgress.value = false
                            _error.value = t
                            t?.log()
                        }
                    )
            }
        }
    }

    private fun toggleFavorite(item: BookItemViewModel) {
        item.takeUnless { it.favoriteUpdateInProgress.get() == true }?.let {
            it.favoriteUpdateInProgress.set(true)
            val isFavorite = it.isFavorite.get() == true
            val task = if (isFavorite) {
                favoriteRecordsUseCase.remove(item.book)
            } else {
                favoriteRecordsUseCase.add(item.book)
            }

            task
                .apiSubscribe(
                    { _ ->
                        it.favoriteUpdateInProgress.set(false)
                        it.isFavorite.set(isFavorite.not())
                    },
                    { t ->
                        t?.log()
                        _databaseError.value = t
                    }
                )
        }
    }

    internal fun retry() {
        lastSearch?.let {
            lastSearch = null
            search(it)
        }
    }

    private fun readBook(book: Book) {
        _startActivity.value = book.bookFragmentUrl.browserIntent()
    }

}