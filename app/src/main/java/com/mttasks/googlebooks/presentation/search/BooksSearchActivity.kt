package com.mttasks.googlebooks.presentation.search

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding4.appcompat.queryTextChanges
import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.ActivityBooksSearchBinding
import com.mttasks.googlebooks.presentation.common.toMessage
import com.mttasks.googlebooks.presentation.common.viewModel
import com.mttasks.googlebooks.presentation.common.withTransaction
import com.mttasks.googlebooks.presentation.search.fragment.SearchBooksListFragment
import com.mttasks.googlebooks.presentation.search.viewmodel.SearchBooksListViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.SearchBooksViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.util.concurrent.TimeUnit
import javax.inject.Inject


/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BooksSearchActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var fragmentContainerId: Int = R.id.fragmentContainer

    private var favoritesCount: Int = 0

    private var searchViewModel: SearchBooksViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val pm = packageManager
        val activityInfo = pm.getActivityInfo(componentName, PackageManager.GET_META_DATA)
        activityInfo.metaData?.getString("activity_label")?.let {
            title = it
        }

        DataBindingUtil.setContentView<ActivityBooksSearchBinding>(
            this,
            R.layout.activity_books_search
        ).apply {
            lifecycleOwner = this@BooksSearchActivity

            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
            supportActionBar?.setDisplayShowTitleEnabled(false)

            searchViewModel =
                this@BooksSearchActivity.viewModel<SearchBooksViewModel>(viewModelFactory).apply {

                    startActivity.observe(this@BooksSearchActivity, Observer {
                        startActivity(it.get(root.context))
                    })

                    favoritesCount.observe(this@BooksSearchActivity, Observer {
                        this@BooksSearchActivity.favoritesCount = it ?: 0
                        invalidateOptionsMenu()
                    })

                    searchView.queryTextChanges().debounce(500L, TimeUnit.MILLISECONDS)
                        .filter { it.length > 2 }
                        .observeOn(io.reactivex.rxjava3.android.schedulers.AndroidSchedulers.mainThread())
                        .subscribe {
                            searchTerm.value = it.toString()
                        }

                    searchTerm.observe(this@BooksSearchActivity, Observer {
                        (supportFragmentManager.findFragmentById(fragmentContainerId) as? SearchBooksListFragment)?.apply {
                            this.viewModel<SearchBooksListViewModel>(viewModelFactory).search(it)
                        }
                    })

                    databaseError.observe(this@BooksSearchActivity, Observer {
                        if (it != null) {
                            Snackbar.make(
                                root,
                                it.toMessage().get(this@BooksSearchActivity),
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    })
                }

            if (savedInstanceState == null) {
                supportFragmentManager.withTransaction {
                    replace(
                        fragmentContainerId,
                        SearchBooksListFragment()
                    )
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_books_search, menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionFavorites -> {
                searchViewModel?.toFavorites()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.actionFavorites)?.isVisible = favoritesCount > 0
        return super.onPrepareOptionsMenu(menu)
    }
}