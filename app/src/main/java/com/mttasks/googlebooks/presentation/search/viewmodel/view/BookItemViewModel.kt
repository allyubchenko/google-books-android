package com.mttasks.googlebooks.presentation.search.viewmodel.view

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.presentation.common.AppString

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class BookItemViewModel(
    val book: com.mttasks.googlebooks.domain.common.model.Book,
    private val clickListener: (BookItemViewModel) -> Unit,
    private val favoriteClickListener: (BookItemViewModel) -> Unit
) : ViewModel() {
    val favoriteUpdateInProgress = ObservableField<Boolean>()
    val isFavorite = ObservableField<Boolean>()

    val authors = AppString {
        if (book.authors.isEmpty().not()) {
            val delimiter = getString(R.string.delimiter_authors)
            getString(R.string.template_authors, book.authors.joinToString(separator = delimiter))
        } else {
            ""
        }
    }

    fun click() {
        clickListener(this)
    }

    fun favoriteClick() {
        favoriteClickListener(this)
    }
}

fun List<com.mttasks.googlebooks.domain.common.model.Book>?.toViewModelList(
    clickListener: (BookItemViewModel) -> Unit,
    favoriteClickListener: (BookItemViewModel) -> Unit
) = this?.takeIf { it.isEmpty().not() }?.map {
    BookItemViewModel(
        it,
        clickListener,
        favoriteClickListener
    )
}