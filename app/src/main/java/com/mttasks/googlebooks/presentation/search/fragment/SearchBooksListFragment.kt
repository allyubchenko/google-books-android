package com.mttasks.googlebooks.presentation.search.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.FragmentListBinding
import com.mttasks.googlebooks.presentation.common.dividerDecorator
import com.mttasks.googlebooks.presentation.common.toMessage
import com.mttasks.googlebooks.presentation.common.viewModel
import com.mttasks.googlebooks.presentation.search.view.listitem.BookSearchListItem
import com.mttasks.googlebooks.presentation.search.viewmodel.SearchBooksListViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.view.BookItemViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class SearchBooksListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val adapter = GroupAdapter<GroupieViewHolder>()

    private var searchViewModel: SearchBooksListViewModel? = null

    private var items: List<BookItemViewModel>? = null
    private var inProgress = false
    private var error: Throwable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DataBindingUtil.inflate<FragmentListBinding>(
        inflater,
        R.layout.fragment_list,
        container,
        false
    ).apply {
        lifecycleOwner = viewLifecycleOwner
        searchViewModel =
            viewModel<SearchBooksListViewModel>(viewModelFactory).apply SearchBooksListViewModel@{
                searchResult.observe(viewLifecycleOwner, Observer {
                    this@SearchBooksListFragment.items = it
                    updateItems()
                })
                inProgress.observe(viewLifecycleOwner, Observer {
                    this@SearchBooksListFragment.inProgress = it == true
                    updateItems()
                })
                error.observe(viewLifecycleOwner, Observer {
                    this@SearchBooksListFragment.error = it
                    updateItems()
                })
                databaseError.observe(viewLifecycleOwner, Observer {
                    if (it != null) {
                        Snackbar.make(root, it.toMessage().get(root.context), Snackbar.LENGTH_SHORT)
                            .show()
                    }
                })
                startActivity.observe(viewLifecycleOwner, Observer {
                    it?.let { activity?.startActivity(it.get(root.context)) }
                })

                recycler.dividerDecorator(LinearLayoutManager.VERTICAL)
                recycler.adapter = this@SearchBooksListFragment.adapter
            }
    }.root

    private fun updateItems() {
        val adapterItems = error?.let {
            listOf(
                com.mttasks.googlebooks.presentation.common.view.listitem.ErrorListItem(
                    com.mttasks.googlebooks.presentation.common.viewmodel.view.ErrorViewModel(
                        error = it,
                        retryDelegate = { searchViewModel?.retry() })
                )
            )
        } ?: inProgress.takeIf { it }?.let {
            listOf(com.mttasks.googlebooks.presentation.common.view.listitem.LoadingListItem())
        } ?: items.let { items ->
            if (items.isNullOrEmpty()) {
                listOf(
                    com.mttasks.googlebooks.presentation.common.view.listitem.MessageListItem(
                        com.mttasks.googlebooks.presentation.common.viewmodel.view.MessageViewModel(
                            com.mttasks.googlebooks.presentation.common.AppString {
                                getString(
                                    R.string.message_search_no_result
                                )
                            })
                    )
                )
            } else {
                items.map {
                    BookSearchListItem(
                        it
                    )
                }
            }
        }

        adapter.update(adapterItems)
    }

}