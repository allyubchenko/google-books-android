package com.mttasks.googlebooks.presentation.common.view.listitem

import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.ViewErrorBinding
import com.mttasks.googlebooks.presentation.common.viewmodel.view.ErrorViewModel
import com.xwray.groupie.databinding.BindableItem

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class ErrorListItem(private val errorViewModel: ErrorViewModel) : BindableItem<ViewErrorBinding>() {
    override fun getLayout() = R.layout.view_error

    override fun bind(viewBinding: ViewErrorBinding, position: Int) {
        viewBinding.vm = errorViewModel
    }
}