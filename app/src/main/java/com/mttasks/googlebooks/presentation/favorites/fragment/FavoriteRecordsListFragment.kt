package com.mttasks.googlebooks.presentation.favorites.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mttasks.googlebooks.R
import com.mttasks.googlebooks.databinding.FragmentListBinding
import com.mttasks.googlebooks.presentation.common.AppString
import com.mttasks.googlebooks.presentation.common.dividerDecorator
import com.mttasks.googlebooks.presentation.common.view.listitem.ErrorListItem
import com.mttasks.googlebooks.presentation.common.view.listitem.LoadingListItem
import com.mttasks.googlebooks.presentation.common.view.listitem.MessageListItem
import com.mttasks.googlebooks.presentation.common.viewModel
import com.mttasks.googlebooks.presentation.common.viewmodel.view.ErrorViewModel
import com.mttasks.googlebooks.presentation.common.viewmodel.view.MessageViewModel
import com.mttasks.googlebooks.presentation.favorites.viewmodel.FavoriteRecordsListViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.view.BookItemViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.DaggerFragment
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class FavoriteRecordsListFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var favoritesViewModel: FavoriteRecordsListViewModel? = null

    private val adapter = GroupAdapter<GroupieViewHolder>()
    private var items: List<BookItemViewModel>? = null
    private var inProgress = false
    private var error: Throwable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DataBindingUtil.inflate<FragmentListBinding>(
        inflater,
        R.layout.fragment_list,
        container,
        false
    ).apply {
        lifecycleOwner = viewLifecycleOwner
        favoritesViewModel = viewModel<FavoriteRecordsListViewModel>(viewModelFactory).apply {
            items.observe(viewLifecycleOwner, Observer {
                this@FavoriteRecordsListFragment.items = it
                updateItems()
            })
            inProgress.observe(viewLifecycleOwner, Observer {
                this@FavoriteRecordsListFragment.inProgress = it == true
                updateItems()
            })
            error.observe(viewLifecycleOwner, Observer {
                this@FavoriteRecordsListFragment.error = it
                updateItems()
            })
            startActivity.observe(viewLifecycleOwner, Observer {
                it?.let { activity?.startActivity(it.get(root.context)) }
            })

            recycler.dividerDecorator(LinearLayoutManager.VERTICAL)
            recycler.adapter = this@FavoriteRecordsListFragment.adapter
        }
    }.root

    private fun updateItems() {
        val adapterItems = error?.let {
            listOf(
                ErrorListItem(
                    ErrorViewModel(
                        error = it,
                        retryDelegate = { favoritesViewModel?.retry() })
                )
            )
        } ?: inProgress.takeIf { it }?.let {
            listOf(LoadingListItem())
        } ?: items.let { items ->
            if (items.isNullOrEmpty()) {
                listOf(
                    MessageListItem(
                        MessageViewModel(
                            AppString {
                                getString(
                                    R.string.message_favorites_no_result
                                )
                            })
                    )
                )
            } else {
                items.map {
                    com.mttasks.googlebooks.presentation.favorites.view.listitem.BookFavoriteListItem(
                        it
                    )
                }
            }
        }
        adapter.update(adapterItems)
    }

}