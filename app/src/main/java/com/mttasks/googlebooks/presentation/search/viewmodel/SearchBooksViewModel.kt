package com.mttasks.googlebooks.presentation.search.viewmodel

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import com.mttasks.googlebooks.domain.favorites.interactor.ManageFavoriteRecordsUseCase
import com.mttasks.googlebooks.presentation.common.AppIntent
import com.mttasks.googlebooks.presentation.favorites.BooksFavoriteActivity
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class SearchBooksViewModel @Inject constructor(
    favoriteRecordsUseCase: ManageFavoriteRecordsUseCase
) : ViewModel() {

    val searchTerm = MutableLiveData<String>()

    private val _startActivity = LiveEvent<AppIntent>()
    val startActivity: LiveData<AppIntent> = _startActivity

    private val _databaseError = LiveEvent<Throwable>()
    val databaseError: LiveData<Throwable> = _databaseError

    private val _favoritesCount =
        LiveDataReactiveStreams.fromPublisher(favoriteRecordsUseCase.count()
            .onErrorReturn {
                _databaseError.postValue(it)
                0
            })
    val favoritesCount: LiveData<Int> = _favoritesCount

    fun toFavorites() {
        _startActivity.value = AppIntent {
            Intent(this, BooksFavoriteActivity::class.java)
        }
    }
}