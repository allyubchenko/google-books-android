package com.mttasks.googlebooks.presentation.common.viewmodel.view

import androidx.lifecycle.ViewModel
import com.mttasks.googlebooks.presentation.common.toMessage

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class ErrorViewModel(
    error: Throwable,
    private val retryDelegate: () -> Unit
) : ViewModel() {

    val message = error.toMessage()

    fun retry() {
        retryDelegate()
    }

}
