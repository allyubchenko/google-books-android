package com.mttasks.googlebooks.presentation.favorites.viewmodel

import androidx.lifecycle.*
import com.hadilq.liveevent.LiveEvent
import com.mttasks.googlebooks.domain.common.model.Book
import com.mttasks.googlebooks.presentation.common.AppIntent
import com.mttasks.googlebooks.presentation.common.apiSubscribe
import com.mttasks.googlebooks.presentation.common.browserIntent
import com.mttasks.googlebooks.presentation.common.log
import com.mttasks.googlebooks.presentation.search.viewmodel.view.BookItemViewModel
import com.mttasks.googlebooks.presentation.search.viewmodel.view.toViewModelList
import javax.inject.Inject

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class FavoriteRecordsListViewModel @Inject constructor(
    private val favoriteRecordsUseCase: com.mttasks.googlebooks.domain.favorites.interactor.ManageFavoriteRecordsUseCase
) : ViewModel() {

    private val _error = LiveEvent<Throwable>()
    val error: LiveData<Throwable> = _error

    private val _startActivity = LiveEvent<AppIntent>()
    val startActivity: LiveData<AppIntent> = _startActivity

    private var itemsStream: LiveData<List<BookItemViewModel>?>? = null
    private val _items = MediatorLiveData<List<BookItemViewModel>>()
    val items: LiveData<List<BookItemViewModel>> = Transformations.map(_items) { it }

    private val _inProgress = MediatorLiveData<Boolean>()
    val inProgress: LiveData<Boolean> = _inProgress

    init {
        updateItems()
    }

    internal fun retry() {
        updateItems()
    }

    private fun updateItems() {
        _inProgress.value = true

        itemsStream?.let { _items.removeSource(it) }
        itemsStream = LiveDataReactiveStreams.fromPublisher(favoriteRecordsUseCase.all()
            .map { items ->
                items.map { it.toBook() }.toViewModelList(
                    clickListener = { readBook(it.book) },
                    favoriteClickListener = {
                        toggleFavorite(it)
                    }
                ).also { mappedItems -> mappedItems?.forEach { it.isFavorite.set(true) } }
                    ?: emptyList()
            }
            .doOnNext {
                _error.postValue(null)
            }
            .onErrorReturn {
                _error.postValue(it)
                emptyList()
            }).apply {
            _items.addSource(this) {
                _items.value = it
                _inProgress.value = false
            }
        }
    }

    private fun readBook(book: Book) {
        _startActivity.value = book.bookFragmentUrl.browserIntent()
    }

    private fun toggleFavorite(item: BookItemViewModel) {
        item.takeUnless { it.favoriteUpdateInProgress.get() == true }?.let {
            it.favoriteUpdateInProgress.set(true)
            val isFavorite = it.isFavorite.get() == true
            val task = if (isFavorite) {
                favoriteRecordsUseCase.remove(it.book)
            } else {
                favoriteRecordsUseCase.add(it.book)
            }

            task
                .apiSubscribe(
                    { _ ->
                        it.favoriteUpdateInProgress.set(false)
                        it.isFavorite.set(isFavorite.not())
                    },
                    { t ->
                        t?.log()
                        _error.value = t
                    }
                )
        }
    }

}

fun com.mttasks.googlebooks.data.favorites.database.entity.FavoriteRecordEntity.toBook() =
    Book(
        id = this.uid,
        thumbUrl = this.thumbUrl,
        title = this.title,
        authors = this.authors,
        bookFragmentUrl = this.bookFragmentUrl
    )