package com.mttasks.googlebooks.presentation.common

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
@BindingAdapter("android:text")
fun TextView.setText(value: AppString?) {
    text = value?.get(context)
}

@Suppress("unused")
@BindingAdapter("selected")
fun View.setSelected(value: Boolean?) {
    isSelected = value == true
}

@BindingAdapter("url")
fun ImageView.setImageUrl(url: String?) {
    Picasso.get().load(url).into(this)
    if (url.isNullOrBlank()) setImageDrawable(null)
}