package com.mttasks.googlebooks

import androidx.multidex.MultiDex
import com.mttasks.googlebooks.di.ApplicationModule
import com.mttasks.googlebooks.di.DaggerGoogleBooksApplicationComponent
import com.mttasks.googlebooks.di.favorites.FavoriteRecordsDataModule
import dagger.android.support.DaggerApplication
import timber.log.Timber

/**
 *  <p>Created by Alexander Lyubchenko &lt;alexander775412@gmail.com&gt; on 31.01.2020.</p>
 */
class GoogleBooksApplication : DaggerApplication() {

    override fun applicationInjector() = DaggerGoogleBooksApplicationComponent.builder()
        .applicationModule(ApplicationModule(this))
        .favoriteRecordsDataModule(FavoriteRecordsDataModule("favorite_records"))
        .build()

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.tag(packageName)
        }
    }
}